# Container based coding environment

![PWA](pwa-screenshot.png)

This image delivers a complete development environment for DevOps tasks. It was created with following design goals in mind:

* Focus on developing automation scripts and container images.
* Deliver a real linux based environment to be as much as compatible as possible to the build, test and production environments.
* Can run in any container orchestration system like docker or k8s.
* Have a ability to build and test container images (docker build, docker run ...)
* Full automated deployment
* Easy to setup
* Every needed tool should be in place (git, ssh-client, kubectl, container runtime, password manager etc.)
* Integrated vscode IDE accessible via a web browser over https, so no local setup is required.
* Integrated filebrowser to ease the exchange of files

## Additional Notes

The web interface from *code-server* is implemented as an PWA (progressive web app). That means that it is a installable app, that behaves like a normal Desktop app (fast load, no browser ui, own icon, start menu entry ...). Install the app by click on the +-Sign in the adress bar:

![PWA](install-pwa.png)

The developer acts as the user *podman*. His home directory is `/podman`. It is very sensible to mount a volume on this location, to hold the state even when the container dies.

Daemon- and root less container development is implemented with [podman](https://podman.io/) and [buildah](https://buildah.io/). They are installed as dropin replacements for the docker-cli. The commands `docker` and `docker-compose` are only sympolic links to `podman` and `podman-compose`. The container musst run privileged to use the container functions.

The OCI container runtime is [crun](https://github.com/containers/crun). The standard [runc](https://github.com/opencontainers/runc) runtime was not runable on some kubernetes clusters.

## Credits

This repo based on several open source projects:

* [Ubuntu Linux](https://ubuntu.com) - base system
* [podman](https://podman.io/) - run containers
* [buildah](https://buildah.io/) - build containers
* [podman-static](https://github.com/mgoltzsche/podman-static) - make podman work inside a container
* [code-server](https://github.com/cdr/code-server) - browser based vscode IDE
* [filebrowser](https://github.com/filebrowser/filebrowser) - a web based file browser

Many thanks for the great work!

## local build

    docker-compose build

## privileged

It is necessary to run the container in privileged mode to use podman.

## deployment

### config via env vars

    CODE_PORT: port to access the web ide, default 6080
    FILEBROWSER_PORT: port to access the filebrowser, default 6090
    CODE_PASSWORD: password for ide and filebrowser (user: podman), default _changemenow_

### local docker

For testing you can simply call `docker-compose`:

    docker-compose up -d

and visit the http://localhost:8080, access the IDE with the password `_changemenow_`.

### kubernetes

Please look at the example manifest file [kubernetes.yaml](kubernetes.yaml) for reference and take care of the comments regarding to

* the Persistent volume claim and storage class
* the base64 encoded secret to set the access password
* the domain name and certificate

