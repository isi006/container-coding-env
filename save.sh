#!/usr/bin/env bash

rm -rf save

mkdir -p save

docker pull isi006/container-coding-env:latest

docker save isi006/container-coding-env:latest | gzip > save/container-coding-env.tar.gzex