# # runc
# FROM docker.io/library/golang:1.14-alpine3.12 AS runc
# ARG RUNC_VERSION=v1.0.0-rc91
# RUN set -eux; \
# 	apk add --no-cache --virtual .build-deps gcc musl-dev libseccomp-dev make git bash; \
# 	git clone --branch ${RUNC_VERSION} https://github.com/opencontainers/runc src/github.com/opencontainers/runc; \
# 	cd src/github.com/opencontainers/runc; \
# 	make static BUILDTAGS='seccomp selinux ambient'; \
# 	mv runc /usr/local/bin/runc; \
# 	rm -rf $GOPATH/src/github.com/opencontainers/runc; \
# 	apk del --purge .build-deps; \
# 	[ "$(ldd /usr/local/bin/runc | wc -l)" -eq 0 ] || (ldd /usr/local/bin/runc; false)

# podman build base
FROM docker.io/library/golang:1.14-alpine3.12 AS podmanbuildbase
RUN apk add --update --no-cache git make gcc pkgconf musl-dev \
	btrfs-progs btrfs-progs-dev libassuan-dev lvm2-dev device-mapper \
	glib-static libc-dev gpgme-dev protobuf-dev protobuf-c-dev \
	libseccomp-dev libselinux-dev ostree-dev openssl iptables bash \
	go-md2man
RUN git clone https://github.com/bats-core/bats-core.git && cd bats-core && ./install.sh /usr/local


# podman (without systemd support)
FROM podmanbuildbase AS podman
RUN apk add --update --no-cache tzdata curl
ARG PODMAN_VERSION=v2.2.1
RUN git clone --branch ${PODMAN_VERSION} https://github.com/containers/podman src/github.com/containers/podman
WORKDIR $GOPATH/src/github.com/containers/podman
RUN make install.tools
RUN set -eux; \
	make bin/podman LDFLAGS_PODMAN="-s -w -extldflags '-static'" BUILDTAGS='seccomp selinux apparmor exclude_graphdriver_devicemapper containers_image_ostree_stub containers_image_openpgp'; \
	mv bin/podman /usr/local/bin/podman; \
	podman --help >/dev/null; \
	[ "$(ldd /usr/local/bin/podman | wc -l)" -eq 0 ] || (ldd /usr/local/bin/podman; false)

# conmon (without systemd support)
FROM podmanbuildbase AS conmon
# conmon 2.0.19 cannot be built currently since alpine does not provide nix package yet
ARG CONMON_VERSION=v2.0.22
RUN git clone --branch ${CONMON_VERSION} https://github.com/containers/conmon.git /conmon
WORKDIR /conmon
RUN set -ex; \
	make git-vars bin/conmon PKG_CONFIG='pkg-config --static' CFLAGS='-std=c99 -Os -Wall -Wextra -Werror -static' LDFLAGS='-s -w -static'; \
	bin/conmon --help >/dev/null

# CNI plugins
FROM podmanbuildbase AS cniplugins
ARG CNI_PLUGIN_VERSION=v0.9.0
RUN git clone --branch=${CNI_PLUGIN_VERSION} https://github.com/containernetworking/plugins /go/src/github.com/containernetworking/plugins
WORKDIR /go/src/github.com/containernetworking/plugins
RUN set -ex; \
	for PLUGINDIR in plugins/ipam/host-local plugins/main/loopback plugins/main/bridge plugins/meta/portmap; do \
		PLUGINBIN=/usr/libexec/cni/$(basename $PLUGINDIR); \
		CGO_ENABLED=0 go build -o $PLUGINBIN -ldflags "-s -w -extldflags '-static'" ./$PLUGINDIR; \
		[ "$(ldd $PLUGINBIN | grep -Ev '^\s+ldd \(0x[0-9a-f]+\)$' | wc -l)" -eq 0 ] || (ldd $PLUGINBIN; false); \
	done

# slirp4netns
FROM podmanbuildbase AS slirp4netns
WORKDIR /
RUN apk add --update --no-cache autoconf automake meson ninja linux-headers libcap-static libcap-dev
# Build libslirp
ARG LIBSLIRP_VERSION=v4.4.0
RUN git clone --branch=${LIBSLIRP_VERSION} https://gitlab.freedesktop.org/slirp/libslirp.git
WORKDIR /libslirp
RUN set -ex; \
	LDFLAGS="-s -w -static" meson --prefix /usr -D default_library=static build; \
	ninja -C build install
# Build slirp4netns
WORKDIR /
ARG SLIRP4NETNS_VERSION=v1.1.8
RUN git clone --branch $SLIRP4NETNS_VERSION https://github.com/rootless-containers/slirp4netns.git
WORKDIR /slirp4netns
RUN set -ex; \
	./autogen.sh; \
	LDFLAGS=-static ./configure --prefix=/usr; \
	make

# fuse-overlayfs (derived from https://github.com/containers/fuse-overlayfs/blob/master/Dockerfile.static)
FROM podmanbuildbase AS fuse-overlayfs
RUN apk add --update --no-cache autoconf automake meson ninja clang g++ eudev-dev fuse3-dev
ARG LIBFUSE_VERSION=fuse-3.10.1
RUN git clone --branch=$LIBFUSE_VERSION https://github.com/libfuse/libfuse /libfuse
WORKDIR /libfuse
RUN set -ex; \
	mkdir build; \
	cd build; \
	LDFLAGS="-lpthread -s -w -static" meson --prefix /usr -D default_library=static .. || (cat /libfuse/build/meson-logs/meson-log.txt; false); \
	ninja; \
	touch /dev/fuse; \
	ninja install; \
	fusermount3 -V
ARG FUSEOVERLAYFS_VERSION=v1.3.0
RUN git clone --branch=$FUSEOVERLAYFS_VERSION https://github.com/containers/fuse-overlayfs /fuse-overlayfs
WORKDIR /fuse-overlayfs
RUN set -ex; \
	sh autogen.sh; \
	LIBS="-ldl" LDFLAGS="-s -w -static" ./configure --prefix /usr; \
	make; \
	make install; \
	fuse-overlayfs --help >/dev/null

# Downloads
FROM docker.io/library/alpine:3.12 AS downloads
RUN apk add --no-cache gnupg
ARG CRUN_VERSION=0.16
RUN set -ex; \
	wget -O /usr/local/bin/crun https://github.com/containers/crun/releases/download/$CRUN_VERSION/crun-${CRUN_VERSION}-linux-amd64-disable-systemd; \
	wget -O /tmp/crun.asc https://github.com/containers/crun/releases/download/$CRUN_VERSION/crun-${CRUN_VERSION}-linux-amd64-disable-systemd.asc; \
	gpg --keyserver ha.pool.sks-keyservers.net --recv-keys 027F3BD58594CA181BB5EC50E4730F97F60286ED; \
	gpg --batch --verify /tmp/crun.asc /usr/local/bin/crun; \
	chmod +x /usr/local/bin/crun; \
	crun --help >/dev/null

# ARG FUSEOVERLAYFS_VERSION=v1.2.0
# RUN set -ex; \
# 	wget -O /usr/local/bin/fuse-overlayfs https://github.com/containers/fuse-overlayfs/releases/download/${FUSEOVERLAYFS_VERSION}/fuse-overlayfs-x86_64; \
# 	chmod +x /usr/local/bin/fuse-overlayfs;

# filebrowser
ARG FILEBROWSER_VERSION=v2.11.0
RUN mkdir -p /apps/filebrowser && cd /apps/filebrowser && \
	wget -O filebrowser.tar.gz https://github.com/filebrowser/filebrowser/releases/download/${FILEBROWSER_VERSION}/linux-amd64-filebrowser.tar.gz	

RUN cd /apps/filebrowser && tar -xzf filebrowser.tar.gz 


####
# Final image
####
FROM ubuntu:20.04
ENV DEBIAN_FRONTEND noninteractive

# Install some important packages
RUN ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime && \
	apt-get update && apt-get install -y --no-install-recommends \
	curl \
	wget \
	vim \
	tzdata \
	ca-certificates \
	iptables \
	uidmap \
	sudo \
	git \
	openssh-client \
	openssl \
	zip \
	unzip \
	jq \
	iproute2 \
	python3 \
	python3-pip \
	fuse3 \
	gettext-base \
	pass \
	iputils-ping \
	&& dpkg-reconfigure --frontend noninteractive tzdata \
	&& rm -rf /var/lib/apt/lists/* 

RUN pip3 install podman-compose yq

# install code server
ARG CODE_SERVER_VERSION=3.6.2
RUN curl -LS -o /code-server.tar.gz https://github.com/cdr/code-server/releases/download/v${CODE_SERVER_VERSION}/code-server-${CODE_SERVER_VERSION}-linux-amd64.tar.gz && \
	tar -xzf /code-server.tar.gz -C / && \
	rm /code-server.tar.gz && \
	mv /code-server-* /code-server

# install the current stable version of kubectl
RUN KUBECTL_VERSION=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt) && \
	curl -LS -o /usr/local/bin/kubectl -L https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
	chmod +x /usr/local/bin/kubectl

# install helm 3
RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
	chmod 700 get_helm.sh && \
	./get_helm.sh && \
	rm get_helm.sh

# install restic
ARG RESTIC_VERSION=0.10.0
RUN	curl -LS https://github.com/restic/restic/releases/download/v${RESTIC_VERSION}/restic_${RESTIC_VERSION}_linux_amd64.bz2 | bzip2 -d > /usr/local/bin/restic && \
	chmod +x /usr/local/bin/restic


# Copy binaries from other images
COPY --from=conmon /conmon/bin/conmon /usr/libexec/podman/conmon
COPY --from=cniplugins /usr/libexec/cni /usr/libexec/cni
COPY --from=fuse-overlayfs /usr/bin/fuse-overlayfs /usr/local/bin/fuse-overlayfs
COPY --from=fuse-overlayfs /usr/bin/fusermount3 /usr/local/bin/fusermount3
COPY --from=slirp4netns /slirp4netns/slirp4netns /usr/local/bin/slirp4netns
COPY --from=podman /usr/local/bin/podman /usr/local/bin/podman
COPY --from=downloads /usr/local/bin/crun /usr/local/bin/crun
# COPY --from=downloads /usr/local/bin/fuse-overlayfs /usr/local/bin/fuse-overlayfs
# COPY --from=runc   /usr/local/bin/runc   /usr/local/bin/runc
COPY --from=downloads /apps/filebrowser/filebrowser /usr/local/bin/filebrowser

COPY cni /etc/cni
COPY containers.conf storage.conf registries.conf policy.json /etc/containers/
COPY 20-start-ssh-agent.sh 30-set-env.sh /etc/profile.d/

RUN set -eux; \
	adduser --disabled-password podman --home /podman -u 100000 --gecos ""; \
	echo 'podman:100001:65536' > /etc/subuid; \
	echo 'podman:100001:65536' > /etc/subgid; \
	ln -s /usr/local/bin/podman /usr/bin/docker; \
	ln -s /usr/local/bin/podman-compose /usr/bin/docker-compose; \
	mkdir -pm 775 /etc/containers /podman /podman/.local/share/containers/storage; \
	chown -R podman:podman /podman; \
	chmod 1777 /podman/.local/share/containers/storage; \
	podman --help >/dev/null; \
	/usr/libexec/podman/conmon --help >/dev/null; \
	slirp4netns --help >/dev/null; \
	fuse-overlayfs --help >/dev/null; \
	usermod -aG sudo podman; \
	echo "ALL ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers


# Add Tini
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

COPY startup.sh /
VOLUME /podman
WORKDIR /podman
ENV HOME=/podman
CMD [ "/startup.sh" ]