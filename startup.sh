#!/usr/bin/env bash

set -e

FILEBROWSER_PORT=${FILEBROWSER_PORT:-6090}
CODE_PORT=${CODE_PORT:-6080}
CODE_PASSWORD=${CODE_PASSWORD:-_changemenow_}

if [ $(id -u) -eq 0 ]; then

	cp -n /etc/skel/.??* /podman/

	mkdir -p /podman/.config/code-server
	mkdir -p /podman/.config/code-server

echo "
bind-addr: 0.0.0.0:${CODE_PORT}
auth: password
password: ${CODE_PASSWORD}
cert: false" > /podman/.config/code-server/config.yaml


	# Make sure podman user owns storage directory
	chown -R podman:podman /podman

	chmod 666 /dev/net/tun
	chmod 666 /dev/fuse 

	FBDB=/podman/.config/filebrowser/filebrowser.db
	
	su - podman -c "rm -f /podman/.config/filebrowser/filebrowser.db && filebrowser config init -d /podman/.config/filebrowser/filebrowser.db  && filebrowser users add podman ${CODE_PASSWORD} -d /podman/.config/filebrowser/filebrowser.db"
	su - podman -c "filebrowser -p ${FILEBROWSER_PORT} -a 0.0.0.0 --username podman -r /podman -d /podman/.config/filebrowser/filebrowser.db &"

	exec su - podman -c /code-server/bin/code-server

else
	# TODO: avoid "No subuid ranges found" and setuid errors - follow rootless improvements: https://github.com/containers/podman/issues/3932
	#   As a solution an OCI hook could be configured that runs proot
	exec "$@"
fi